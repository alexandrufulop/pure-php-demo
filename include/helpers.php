<?php
/**
 *
 * Project: video-dk
 * Generated: 05-03-2018 @ 8:47 PM
 *
 * Created by:  Mr. FÜLÖP
 */

const BITLY_USER = 'xxx';
const BITLY_KEY = 'xxx';

/* make a URL small */
function make_bitly_url($url,$login=BITLY_USER,$appkey=BITLY_KEY,$format = 'json',$version = '3')
{
    //create the URL
    $bitly = 'http://api.bit.ly/shorten?version='.$version.'&longUrl='.urlencode($url).'&login='.$login.'&apiKey='.$appkey.'&format='.$format;

    //get the url
    //could also use cURL here
    $response = file_get_contents($bitly);

    //parse depending on desired format
    if(strtolower($format) == 'json')
    {
        $json = @json_decode($response,true);
        return $json['results'][$url]['shortUrl'];
    }
    else //xml
        {
            $xml = simplexml_load_string($response);
            return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
        }
}

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-.]/', '', $string); // Removes special chars.
}