<?php
/**
 *
 * Project: video-dk
 * Generated: 21-02-2018 @ 1:36 AM
 *
 * Created by:  Mr. FÜLÖP
 */

namespace   Onlinepromoters;

require('../vendor/autoload.php');
require('../include/helpers.php');
require('../include/vars.php');

use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use Kunnu\Dropbox\DropboxFile;


ini_set('max_execution_time', 600); //10 minutes
filter_input(INPUT_GET,'f',FILTER_SANITIZE_STRING,!FILTER_FLAG_STRIP_LOW);
$get = trim($_GET['f']);
$filename=isset($_GET['f']) ? $_GET['f'] : '';
$pathToLocalFile = UPLOAD_DIR.'/'.$filename;
if(empty($filename) or  !file_exists($pathToLocalFile)) exit('No file specified for backup!');

/* Logging backups */
$log = fopen("../logs/backup.log", "a") or error_log("Could not open the backup log file in",0);
$txt= "\n====Backup initiated for: ${filename} ====";

//Configure Dropbox service
$app = new DropboxApp('xxx', 'xxx','xxx');
$dropbox = new Dropbox($app);

if(DEBUG){
    error_log("Backup initiated for ${filename} \n Dropbox object ".print_r($dropbox,true), 0);
}


$dropboxFile = new DropboxFile($pathToLocalFile);

/* auto upload by size */
$file = $dropbox->upload($dropboxFile,'/'.$filename, ['autorename' => true]);

if(DEBUG){
    error_log("File upload data ".print_r($file,true), 0);
}

//Uploaded File
//$uploaded_name = $file->getName();
$msg = "\nThe file '${filename}' has been backed up on dropbox.";
echo $msg;

$txt.=$msg; //writting to log too

//Get File Metadata
$link = $dropbox->getTemporaryLink('/'.$filename)->getLink();

$msg="\n====Backup finished for ${filename} ====";

if(isset($link) && filter_var($link, FILTER_VALIDATE_URL))
{
    //Shorten URL with bit.ly
    if(function_exists('make_bitly_url'))
    {
        /* usage */
        if($short_url = make_bitly_url($link))
        {
            $txt.="\nThe short url for the file is: ${short_url}"; //writting to log too
        }
        else
            {
                error_log("Bitly short url error:".print_r($short_url), 0);
            }
    }

    $txt.="\nThe temporary url on dropbox is: ${link}"; //writting to log too
    $txt.=$msg;
}

//todo remove uploaded files on the servers as soon as the files are backed up on dropbox

fwrite($log, $txt);
fclose($log);
error_log($msg,0);
