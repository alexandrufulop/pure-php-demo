<?php

namespace Onlinepromoters;

require('../include/vars.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Upload complete</title>
</head>
<body style="font: 13px Verdana; background: #eee; color: #333">
<h1> Upload complete</h1>
<a href="/" title="New large file upload"> New upload</a>

<?php
if($files = @scandir(UPLOAD_DIR))
{
    if(count($files)>3)
    {
        echo "\n
        <p> Your upload(s) have been completed successfully!</p>";
        foreach ($files as $file)
        {
            if($file !== '.' && $file !== '..' && $file !== '.htaccess' && $file !== __FILE__ && $file !== '.gitignore')
            {
                echo "<li><a href='" .UPLOAD_DIR. "/${file}'>${file}</a> | <a href='backup.php?f=${file}' target='_blank' title='Manual backup this file to dropbox'>Backup now</a></li>";
            }
        }
        echo "</ul>";

    }
    else
        {
            echo "<p>No files uploaded!</p>";
        }

}
else
    {
        echo "<p>No files uploaded!</p>";
        echo "<p><a href='/'>Click here</a> to go to the file uploader</a></p>";
    }
?>

</body>
</html>