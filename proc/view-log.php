<?php
/**
 *
 * Project: video-dk
 * Generated: 21-02-2018 @ 10:18 PM
 *
 * Created by:  Mr. FÜLÖP
 */
$page = $_SERVER['PHP_SELF'];
$sec = 10;
$text = 'Please wait...<br />Leave this page open to get info about the background backup processes...<br />This page reloads every '.$sec.' seconds';
$reload = true;

$log = file_get_contents('../logs/backup.log');
if($log){
    $text = nl2br($log);
    $reload = false;
}

?>
<html>
<head>
<?php
    echo "<meta http-equiv='refresh' content='${sec};URL=${page}' />";
?>
</head>
<body>
<?php
print("<pre>${text}</pre>");
?>

</body>
</html>



